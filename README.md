# Gitlab CI slapper

A script for detecting and restarting stuck CI jobs in a GitLab project.

## Requirements

To wit, this scripts requires [gl](https://gitlab.com/arvidnl/gl), jq, curl and csvkit.

## Usage

Each run of the script only checks if there are any stuck jobs compared to the last run.
To check for stuck jobs continuously, you can use `watch`:

```
PRIVATE_TOKEN=$(glt) ./slapper.sh
```

This will run the script every 60 seconds. The script prints a table
of the currently set of running jobs to stdout, sorted by the column
`since_last_update`. This column contains the seconds since the last
time the script detected that this job advanced.

If a job is detected as stuck, then it's name will be prefixed by
`/!\` and it will be canceled and then retried.

In addition to the table printed on stdout, the script prints log
messages to `/tmp/gitlab-ci-slapper/log`. In particular, it will print
the id of each retried job:

```
tail -f /tmp/gitlab-ci-slapper/log
```

## Usage in Docker

```
docker run -it --env PRIVATE_TOKEN=<MY_ACCESS_TOKEN> --env DRY_RUN_RETRY=1 gitlab-ci-slapper
```

## Definition of stuck jobs

A job is considered stuck when it's [log
(trace)](https://docs.gitlab.com/ee/api/jobs.html#get-a-log-file) has
been in the same section for more than 5 minutes, and that section is
not `step_script`.

## Configuration

The script reads the following environment variables:

 - `PRIVATE_TOKEN`: a GitLab personal access token. Required.
 - `GL_PIPELINE_AUTHOR`: if set, only jobs triggered by this GitLab
   user is monitored.
 - `DRY_RUN_RETRY`: If set to non-zero, the script will only print the
   ID of stuck job, and will not attempt to retry them.
                             
