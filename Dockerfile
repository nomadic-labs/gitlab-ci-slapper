FROM debian:sid

WORKDIR /slapper

RUN apt update && apt install jq curl csvkit tmux watch git --yes

RUN git clone https://gitlab.com/arvidnl/gl.git

RUN ln -s /slapper/gl/gl /usr/local/bin/gl

COPY slapper.sh start-tmux.sh start.sh ./

ENTRYPOINT ["./slapper.sh"]
