#!/bin/sh

tmux new -d -s slapper
tmux new-session -d -s slapper -n status './slapper.sh'
tmux new-window -d -n log 'sleep 10; tail -f /tmp/gitlab-ci-slapper/log'
tmux attach-session -d -t slapper
