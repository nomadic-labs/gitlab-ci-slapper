#!/bin/sh

set -eu

if [ -n "${TRACE:-}" ]; then set -x; fi

usage() {
    cat << EOT
Usage: $0

This script detects and restarts stuck CI jobs in the GitLab project 'tezos/tezos'.

If set, only jobs triggered by GL_PIPELINE_AUTHOR are monitored.

Requires setting a GitLab private token in the environment variable PRIVATE_TOKEN.

If DRY_RUN_RETRY is set to 1, the jos are not retried.

Log is printed to /tmp/gitlab-ci-slapper/log.

Requirements: gl, jq, curl, csvkit.
EOT
    exit 1
}

red='\033[0;31m'
nc='\033[0m' # No Color
color_red() {
    printf "${red}%s${nc}" "$*"
}

url_encode() {
    jq -rn --arg x "$1" '$x|@uri'
}

if [ "${1:-}" = "--help" ]; then
    usage
fi

gitlab_project="tezos/tezos"
sleep_interval="${SLEEP_INTERVAL:-60}"

log_dir=/tmp/gitlab-ci-slapper/
mkdir -p $log_dir
touch $log_dir/retries

log_file=/tmp/gitlab-ci-slapper/log
touch "$log_file"
log() {
    f_d=$(date --iso=seconds)
    echo "[${f_d}] $*" | tee -a $log_file
}

gitlab_curl() {
    curl --silent --fail-with-body --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "$@"
}

dry_run_retry=${DRY_RUN_RETRY:-0}
retry() {
    pipeline_id="$1"
    job_id="$2"
    job_name="$3"

    retries=$(grep -c "$pipeline_id:$job_name" $log_dir/retries || true)
    if [ "$retries" -lt 2 ]; then
        if [ "$dry_run_retry" = "0" ]; then
            cancellation=$(mktemp)
            retry_file=$(mktemp)
            gitlab_curl --request POST "https://gitlab.com/api/v4/projects/$(url_encode $gitlab_project)/jobs/${job_id}/cancel" > "$cancellation"
            new_status=$(jq -r .status < "$cancellation")
            gitlab_curl --request POST "https://gitlab.com/api/v4/projects/$(url_encode $gitlab_project)/jobs/${job_id}/retry" > "$retry_file"
            new_id=$(jq -r .id < "$retry_file")
        else
            log gitlab_curl --request POST "https://gitlab.com/api/v4/projects/$(url_encode $gitlab_project)/jobs/${job_id}/cancel"
            new_status="dry_cancel"
            log gitlab_curl --request POST "https://gitlab.com/api/v4/projects/$(url_encode $gitlab_project)/jobs/${job_id}/retry"
            new_id="dry_new_Id"
        fi
        echo "$pipeline_id:$job_name" >> $log_dir/retries
        log "Cancelled job #${job_id} (${job_name}) with ${retries} previous retries, now has status '$new_status'. Retried job id: #${new_id}"
    else
        log "Job #${job_name} in pipeline ${pipeline_id} has already been retried twice, ignoring."
    fi
}

job_row() {
    pipeline_id="$1"
    job_id="$2"
    job=$(mktemp)
    jq --argjson job_id "$job_id" 'map(select(.id == $job_id))[0]' < "$jobs" > "$job"
    job_name=$(jq -r .name < "$job")
    job_web_url=$(jq -r .web_url < "$job")
    job_duration=$(jq -r .duration < "$job")
    job_created_at=$(jq -r .created_at < "$job")

    # check when the trace was last updated
    old_trace=${log_dir}${job_id}
    trace=$(mktemp)
    gl --project "$gitlab_project" "jobs/${job_id}/trace" > "$trace"
    if [ -f "$old_trace" ]; then
        if ! diff -q "$old_trace" "$trace" > /dev/null; then
            # The trace has updated
            mv "$trace" "$old_trace"
        else
            rm "$trace"
        fi
    else
        # there was no old trace
        mv "$trace" "$old_trace"
    fi
    job_last_updated_epoch=$(stat -c '%Y' "$old_trace")
    job_last_updated=$(date --iso-8601=seconds --date=@"$job_last_updated_epoch")
    job_since_last_update=$(($(date +%s) - job_last_updated_epoch))
    job_section=$(grep -o -P 'section_start:\d+:\w+' < "$old_trace" | cut -d: -f3 | tail -n1)

    if { [ ${job_since_last_update} -gt 300 ] && [ "$job_section" != "step_script" ]; }; then
        retry "$pipeline_id" "$job_id" "$job_name"
        job_name=$(printf "/!\ %s" "$job_name")
    else
        job_name=$(printf "%s" "$job_name")
    fi
    printf "#%d,%s,%s,%.0f,%s,%s,%s,%.0f\n" \
        "${pipeline_id}" \
        "${job_name}" \
        "${job_web_url}" \
        "${job_duration}" \
        "${job_created_at}" \
        "${job_last_updated}" \
        "${job_section}" \
        "${job_since_last_update}"
}

get_pipelines() {
    if [ -n "${GL_PIPELINE_AUTHOR:-}" ]; then
        url="pipelines/?username=${GL_PIPELINE_AUTHOR}&scope=running&source=merge_request_event"
    else
        url='pipelines/?scope=running&source=merge_request_event'
    fi
    gl --project "$gitlab_project" "$url"
}

while true; do
    log "Looking for stuck jobs..."
    job_rows=$(mktemp)
    echo "pipeline_id,name,web_url,duration,created_at,last_updated,section,since_last_update" > "$job_rows"
    pipelines=$(mktemp)
    get_pipelines > "$pipelines"
    for pipeline_id in $(jq 'map(.id)[]' < "$pipelines"); do
        jobs=$(mktemp)
        gl --project "$gitlab_project" "pipelines/${pipeline_id}/jobs?scope=running&per_page=200" > "$jobs"
        for job_id in $(jq 'map(.id)[]' < "$jobs"); do
            job_row "${pipeline_id}" "${job_id}" >> "$job_rows" &
            # break
        done
        # break
    done
    wait

    # cat $job_rows
    # csvsort -d"," -r -c since_last_update < "$job_rows" | csvlook -d"," -I # | head -n 50

    sleep "$sleep_interval"
done

# echo ""
# echo "---------"
# echo ""
# echo "Log: "
# tail -n 50 "$log_file"
